package repository;

import java.util.Collection;

import domain.Account;
import domain.Bank;
import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {
	Collection<Sede> findByBank(Long bankId);	
}
