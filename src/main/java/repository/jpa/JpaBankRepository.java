package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AccountRepository;
import repository.BankRepository;
import domain.Account;
import domain.Bank;

@Repository
public class JpaBankRepository extends JpaBaseRepository<Bank, Long> implements
		BankRepository {

	@Override
	public Bank findByName(String name) {
		String jpaQuery = "SELECT b FROM Bank b WHERE b.name = :name";
		TypedQuery<Bank> query = entityManager.createQuery(jpaQuery, Bank.class);
		query.setParameter("name", name);
		return getFirstResult(query);		
	}

}
