package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AccountRepository;
import repository.BankRepository;
import repository.SedeRepository;
import domain.Account;
import domain.Bank;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {

	@Override
	public Collection<Sede> findByBank(Long bankId) {
		String jpaQuery = "SELECT s FROM Sede WHERE s.bank = :bank";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("bank", bankId);
		return query.getResultList();
	}

}
