package repository;

import java.util.Collection;

import domain.Account;
import domain.Bank;

public interface BankRepository extends BaseRepository<Bank, Long> {
	Bank findByName(String name);	
}
