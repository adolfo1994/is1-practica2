package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_bank", indexes = { @Index(columnList = "name") })
public class Bank implements BaseEntity<Long>{
	
	@Id
	@SequenceGenerator(name = "bank_id_generator", sequenceName = "bank_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_id_generator")
	private Long id;
	
	@Column(unique = true, nullable = false, updatable = false)
	private String name;
	
	@OneToMany(mappedBy = "bank")
	private Collection<Sede> sedes;
	
	@OneToMany(mappedBy = "bank")
	private Collection<Account> accounts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Sede> getSedes() {
		return sedes;
	}

	public void setSedes(Collection<Sede> sedes) {
		this.sedes = sedes;
	}

	public Collection<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Collection<Account> accounts) {
		this.accounts = accounts;
	}
	
	

}
