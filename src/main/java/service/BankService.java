package service;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.AccountRepository;
import repository.BankRepository;
import repository.PersonRepository;
import domain.Account;
import domain.Bank;
import domain.Person;

@Service
public class BankService {
	
	@Autowired
	BankRepository bankRepository;

	@Transactional
	public void save(Bank bank) {
		bankRepository.persist(bank);
	}
	
	@Transactional
	public void creatBank(){
		return;
	}
}
